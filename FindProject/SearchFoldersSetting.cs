﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FindProject
{
    public partial class SearchFoldersSetting : Form
    {
        public SearchFoldersSetting()
        {
            InitializeComponent();
            LoadContent();
        }

        public Action TreeViewRefresh;

        public void LoadContent()
        {
            StringBuilder sb = new StringBuilder();
            AppConfig.Options.SearchFolders.ForEach(item =>
            {
                sb.Append(item + "\n");
            });
            this.richTxtContent.Text = sb.ToString();
        }

        /// <summary>
        /// 通知FindProject 窗体重新加载目录树
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SearchFolders_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (TreeViewRefresh != null)
            {
                TreeViewRefresh();
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadContent();
            MessageBox.Show("加载成功", "成功", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AppConfig.Options.SearchFolders.Clear();
            this.richTxtContent.Text
                .Split('\n')
                .ToList()
                .ForEach(item =>
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        if (!Directory.Exists(item))
                        {
                            MessageBox.Show($"文件夹不存在：{item}", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            AppConfig.Options.SearchFolders.Add(item);
                        }
                    }
                });
            AppConfig.UpdateSection("SearchFolders", AppConfig.Options.SearchFolders);
            MessageBox.Show("保存成功", "成功", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
