﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FindProject.Common;
using FindProject.Model;

namespace FindProject
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
            LoadTree(); // 加载目录树
            this.containerBody.SplitterDistance = this.containerBody.Width / 2;
            UpdateButtonsState();
            this.treeView1.ImageList = this.imageList1;
            this.treeView2.ImageList = this.imageList1;
        }

        /// <summary>
        /// 搜索目录
        /// </summary>
        private static List<string> SearchFolders = AppConfig.Options.SearchFolders;

        /// <summary>
        /// 根节点
        /// </summary>
        private static TreeNode Root;

        /// <summary>
        /// 目录
        /// </summary>
        private static Dictionary<string, TreeNode> DirecotryTreeNode = new Dictionary<string, TreeNode>();

        /// <summary>
        /// 选择项目路径
        /// </summary>
        private string SelectedPath = string.Empty;

        /// <summary>
        /// 选择VS路径
        /// </summary>
        private string SelectedVsPath = string.Empty;

        #region 事件
        private void Main_Shown(object sender, EventArgs e)
        {
            this.textBox1.Focus();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnSearch_Click(sender, e);
            }
        }
        #endregion

        #region TreeView 操作
        private const string TREE_ROOT_NAME = "查询结果";
        private const string TREE_PROJECT_ROOT_NAME = "项目目录";

        /// <summary>
        /// 加载目录树
        /// </summary>
        public void LoadTree()
        {
            // 初始化
            this.treeView1.Nodes.Clear();
            DirecotryTreeNode = new Dictionary<string, TreeNode>();
            SearchFolders = AppConfig.Options.SearchFolders;

            this.treeView1.Nodes.Add(new TreeNode() { Text = TREE_ROOT_NAME });
            Root = this.treeView1.Nodes[0];
            var index = 0;
            SearchFolders.ForEach(item =>
            {
                var node = new TreeNode()
                {
                    Text = item,
                    ToolTipText = item,
                    ImageIndex = 0,
                    SelectedImageIndex = 0
                };
                Root.Nodes.Add(node);
                DirecotryTreeNode.Add(item, Root.Nodes[index]);
                index++;
            });
            this.treeView1.ExpandAll();
        }

        /// <summary>
        /// 加载项目树
        /// </summary>
        /// <param name="projectPath"></param>
        private void LoadProjectTree(string projectPath)
        {
            this.treeView2.Nodes.Clear();
            this.treeView2.Nodes.Add(TREE_PROJECT_ROOT_NAME);
            var root = this.treeView2.Nodes[0];
            var files = Directory.GetFiles(projectPath);
            var directories = Directory.GetDirectories(projectPath);
            var matchItems = MatchVS(projectPath);

            for (int i = 0; i < files.Length; i++)
            {
                var node = new TreeNode()
                {
                    Text = new FileInfo(files[i]).Name,
                    ToolTipText = files[i],
                };
                root.Nodes.Add(node);
                foreach (var item in matchItems)
                {
                    if (item == files[i])
                    {
                        root.Nodes[i].ForeColor = Color.FromArgb(128, 0, 128);
                        root.Nodes[i].SelectedImageIndex = 1;
                        root.Nodes[i].ImageIndex = 1;
                    }
                    else
                    {
                        root.Nodes[i].SelectedImageIndex = 2;
                        root.Nodes[i].ImageIndex = 2;
                    }
                }
            }

            var count = files.Length;
            foreach (var dir in directories)
            {
                var node = new TreeNode()
                {
                    Text = new DirectoryInfo(dir).Name,
                    ToolTipText = dir
                };
                root.Nodes.Add(node);
                foreach (var item in matchItems)
                {
                    var arr = item.Split('\\').ToList();
                    arr.Remove(arr.Last());
                    if (string.Join('\\', arr) == dir)
                    {
                        var childNode = new TreeNode()
                        {
                            Text = new FileInfo(item).Name,
                            ToolTipText = new FileInfo(item).FullName,
                            ForeColor = Color.FromArgb(128, 0, 128),
                            SelectedImageIndex = 1,
                            ImageIndex = 1,
                        };
                        root.Nodes[count].Nodes.Add(childNode);
                    }
                }
                count++;
            }
            this.treeView2.ExpandAll();
        }

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            var selectedNode = e.Node;
            var nodeName = selectedNode.ToolTipText;
            if (nodeName == TREE_ROOT_NAME || string.IsNullOrEmpty(nodeName))
            {
                SelectedPath = "";
                return;
            }
            var folderInfo = FolderHelper.GetFolderInfo(nodeName);
            SelectedPath = folderInfo.FolderPath;
            SelectedVsPath = "";
            // 选择的是项目目录
            if (!SearchFolders.Contains(SelectedPath))
            {
                this.label2.Text = "项目：" + SelectedPath.Split('\\').Last();
                LoadProjectTree(SelectedPath);
            }
            else
            {
                this.label2.Text = "项目：";
                this.treeView2.Nodes.Clear();
            }
            UpdateButtonsState();
        }

        private void treeView2_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            var selectedNode = e.Node;
            var nodeName = selectedNode.ToolTipText;
            SelectedVsPath = "";

            if (nodeName == TREE_PROJECT_ROOT_NAME || string.IsNullOrEmpty(nodeName))
            {
                return;
            }

            if (nodeName.EndsWith("sln"))
            {
                SelectedVsPath = nodeName;
                UpdateButtonsState(false);
            }
            else if (File.Exists(nodeName))
            {
                UpdateButtonsState(false);
            }
            else
            {
                SelectedPath = nodeName;
                UpdateButtonsState();
            }
        }

        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            System.Diagnostics.Process.Start("explorer.exe", e.Node.ToolTipText);
        }

        private void treeView2_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (Path.GetExtension(e.Node.ToolTipText) == ".sln")
            {
                SelectedVsPath = e.Node.ToolTipText;
                var exePath = GetDevenvPath(SelectedVsPath);
                var path = SelectedVsPath;
                System.Diagnostics.Process.Start(exePath, $@"""{path}""");
            }
            else
            {
                System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo(e.Node.ToolTipText) { UseShellExecute = true });
            }
        }
        #endregion

        #region 菜单栏操作
        private void 检索路径ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new SearchFoldersSetting();
            form.TreeViewRefresh = () => this.LoadTree();
            form.Show();
        }

        private void 默认解决方案路径ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new SlnPathSetting();
            form.StartPosition = FormStartPosition.CenterScreen;
            form.Show();
        }

        private void 新增检索目录ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            // 打开目录选择器
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "请选择项目目录";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                var folderPath = dialog.SelectedPath;
                if (SearchFolders.Contains(folderPath))
                {
                    MessageBox.Show("请勿重复添加", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                SearchFolders.Add(folderPath);
                // 更新 appsettings
                AppConfig.UpdateSection("SearchFolders", SearchFolders);
                LoadTree();
            }
        }
        #endregion

        #region 操作
        /// <summary>
        /// 打开文件夹
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExplorer_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(SelectedPath))
            {
                MessageBox.Show("请选择文件夹", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            System.Diagnostics.Process.Start("explorer.exe", SelectedPath);
        }

        /// <summary>
        /// 检索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            LoadTree();

            var count = 0;
            foreach (var dir in SearchFolders)
            {
                string[] folders = Directory.GetDirectories(dir);
                foreach (var folder in folders)
                {
                    if (folder.Contains(this.textBox1.Text))
                    {
                        count++;
                        var node = new TreeNode()
                        {
                            Text = new DirectoryInfo(folder).Name,
                            ToolTipText = folder,
                            ForeColor = Color.FromArgb(0, 0, 139),
                            ImageIndex = 0,
                            SelectedImageIndex = 0
                        };
                        DirecotryTreeNode[dir].Nodes.Add(node);
                    }
                }
            }
            this.treeView1.ExpandAll();
            this.SelectedPath = "";
            this.SelectedVsPath = "";
            this.searchResultLabel.Text = $"搜索结果：查询到{count}个项目";
        }

        /// <summary>
        /// 打开解决方案
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOpenVS_Click(object sender, EventArgs e)
        {
            var exePath = GetDevenvPath(SelectedVsPath);
            var path = SelectedVsPath;
            System.Diagnostics.Process.Start(exePath, $@"""{path}""");
        }
        #endregion

        private static List<string> GetMatchProject(List<string> pathList)
        {
            var filter = pathList.Where(item => File.Exists(item));
            if (filter.Count() == 1)
            {
                return new List<string>() { filter.First() };
            }
            else if (filter.Count() == 0)
            {
                return new List<string>();
            }
            return filter.ToList();
        }

        private void UpdateButtonsState(bool isFolder = true)
        {
            this.btnExplorer.Enabled = !string.IsNullOrEmpty(SelectedPath) && isFolder;
            this.btnOpenVS.Enabled = !string.IsNullOrEmpty(SelectedVsPath) && !isFolder;
        }

        private List<string> MatchVS(string projectPath)
        {
            string path = string.Empty;
            var result = new List<string>();

            if (string.IsNullOrEmpty(projectPath) || SearchFolders.Contains(projectPath))
            {
                return result;
            }

            var existItem = AppConfig.Options.SelectedPath.Find(item => item.ProjectPath == projectPath);
            if (existItem != null)
            {
                path = existItem.SlnPath;
                if (File.Exists(path))
                {
                    result.Add(path);
                }
            }
            else
            {
                return GetMatchProject(FolderHelper.GetProjectPathList(projectPath));
            }
            return result;
        }

        private string GetDevenvPath(string projectPath)
        {
            var exePath = string.Empty;
            using (var stream = File.OpenRead(projectPath))
            {
                using (var reader = new StreamReader(stream))
                {
                    var content = reader.ReadToEnd();
                    if (content.Contains("Format Version 10.00"))
                    {
                        exePath = AppConfig.Options.DevenvPath.ProjectVersion10;
                    }
                    else if (content.Contains("Format Version 11.00"))
                    {
                        exePath = AppConfig.Options.DevenvPath.ProjectVersion11;
                    }
                    else if (content.Contains("Format Version 12.00"))
                    {
                        exePath = AppConfig.Options.DevenvPath.ProjectVersion12;
                    }
                    else
                    {
                        exePath = AppConfig.Options.DevenvPath.Default;
                    }
                }
            }
            return exePath;
        }
    }
}
