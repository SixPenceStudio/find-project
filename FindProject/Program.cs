using FindProject.Impements;
using FindProject.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Configuration;
using System.Runtime.InteropServices;

namespace FindProject
{
    internal static class Program
    {
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool IsIconic(IntPtr hWnd);

        private const int SW_RESTORE = 9;

        static Mutex mutex = new Mutex(true, "YourUniqueApplicationName");

        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (mutex.WaitOne(TimeSpan.Zero, true))
            {
                ApplicationConfiguration.Initialize();
                var services = new ServiceCollection();
                ConfigureServices(services);
                var serviceProvider = services.BuildServiceProvider();
                AppContext.Configure(serviceProvider);
                Application.Run(serviceProvider.GetRequiredService<Main>());
            }
            else
            {
                ActivateExistingWindow();
            }
        }

        static void ActivateExistingWindow()
        {
            var current = System.Diagnostics.Process.GetCurrentProcess();
            foreach (var process in System.Diagnostics.Process.GetProcessesByName(current.ProcessName))
            {
                if (process.Id != current.Id)
                {
                    IntPtr hWnd = process.MainWindowHandle;
                    if (IsIconic(hWnd))  // 如果窗口是最小化的
                    {
                        ShowWindow(hWnd, SW_RESTORE);  // 恢复窗口
                    }
                    SetForegroundWindow(hWnd);  // 将窗口带到前台
                    break;
                }
            }
        }

        public static void ConfigureServices(IServiceCollection services)
        {
            // 添加匹配项目路径的实现类
            services.AddSingleton<IMatchProjectPath, MatchDocareInterfaceV35>();
            services.AddSingleton<IMatchProjectPath, MatchDocareInterfaceV4>();
            services.AddSingleton<IMatchProjectPath, MatchDocareInterfaceV6>();
            services.AddSingleton<IMatchProjectPath, MatchEmergencyMdtApiV2>();
            services.AddSingleton<IMatchProjectPath, MatchEmergencyMdtApiV3>();
            services.AddSingleton<IMatchProjectPath, MatchEmergencyMdtApiV4>();
            services.AddSingleton<IMatchProjectPath, MatchIntegrationPortal>();
            services.AddSingleton<IMatchProjectPath, MatchDoCareInterFaceCDAV1>();
            services.AddScoped(typeof(Main));
        }
    }
}