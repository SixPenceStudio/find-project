﻿using FindProject.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindProject.Impements
{
    internal class MatchDocareInterfaceV35 : IMatchProjectPath
    {
        public string MatchProject(string fullPath, string projectFolderName)
        {
            if (projectFolderName == "NET-WS-V3.5")
            {
                return Path.Combine(fullPath, "DocareInterfaceV3.sln");
            }
            return "";
        }
    }
}
