﻿using FindProject.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindProject.Impements
{
    internal class MatchIntegrationPortal : IMatchProjectPath
    {
        public string MatchProject(string fullPath, string projectFolderName)
        {
            if (projectFolderName == "IntegrationPortal")
            {
                return Path.Combine(fullPath, "IntegrationPortal.sln");
            }
            return "";
        }
    }
}
