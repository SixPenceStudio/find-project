﻿using FindProject.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindProject.Impements
{
    internal class MatchEmergencyMdtApiV2 : IMatchProjectPath
    {
        public string MatchProject(string fullPath, string projectFolderName)
        {
            if (!string.IsNullOrEmpty(projectFolderName))
                return "";

            var fileList = Directory.GetFiles(fullPath, "*.sln", SearchOption.TopDirectoryOnly).ToList();
            if (Directory.Exists(Path.Combine(fullPath, "DoCare")))
            {
                fileList.AddRange(Directory.GetFiles(Path.Combine(fullPath, "DoCare"), "*.sln", SearchOption.TopDirectoryOnly).ToList());
            }
            if (Directory.Exists(Path.Combine(fullPath, "DoCarePlat")))
            {
                fileList.AddRange(Directory.GetFiles(Path.Combine(fullPath, "DoCarePlat"), "*.sln", SearchOption.TopDirectoryOnly).ToList());
            }
            if (fileList.Count > 0)
            {
                return fileList.First();
            }
            return "";
        }
    }
}
