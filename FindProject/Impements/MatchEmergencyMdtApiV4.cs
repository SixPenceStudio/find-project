﻿using FindProject.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindProject.Impements
{
    internal class MatchEmergencyMdtApiV4 : IMatchProjectPath
    {
        public string MatchProject(string fullPath, string projectFolderName)
        {
            if (projectFolderName == "MedicalSystem.EmergencyMdtApi")
            {
                return Path.Combine(fullPath, "MedicalSystem.EmergencyMdtApi.sln");
            }
            return "";
        }
    }
}
