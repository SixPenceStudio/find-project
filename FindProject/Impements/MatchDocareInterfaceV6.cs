﻿using FindProject.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindProject.Impements
{
    internal class MatchDocareInterfaceV6 : IMatchProjectPath
    {
        public string MatchProject(string fullPath, string projectFolderName)
        {
            if (projectFolderName == "NET_WS_V6")
            {
                return Path.Combine(fullPath, "DocareInterfaceV6.sln");
            }
            return "";
        }
    }
}
