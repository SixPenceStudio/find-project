﻿using FindProject.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindProject.Impements
{
    internal class MatchDoCareInterFaceCDAV1 : IMatchProjectPath
    {
        public string MatchProject(string fullPath, string projectFolderName)
        {
            if (projectFolderName == "WebApi")
            {
                return Path.Combine(fullPath, "doCarePlatform.sln");
            }
            return "";
        }
    }
}
