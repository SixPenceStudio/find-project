﻿using FindProject.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindProject.Impements
{
    internal class MatchDocareInterfaceV4 : IMatchProjectPath
    {
        public string MatchProject(string fullPath, string projectFolderName)
        {
            if (projectFolderName == "NET-WS-V4" || projectFolderName.StartsWith("NET-WS-V4"))
            {
                return Path.Combine(fullPath, "DocareInterfaceV4.sln");
            }
            return "";
        }
    }
}
