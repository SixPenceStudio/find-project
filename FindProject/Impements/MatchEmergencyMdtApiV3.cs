﻿using FindProject.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindProject.Impements
{
    internal class MatchEmergencyMdtApiV3 : IMatchProjectPath
    {
        public string MatchProject(string fullPath, string projectFolderName)
        {
            if (!string.IsNullOrEmpty(projectFolderName))
                return "";

            var fileList = Directory.GetFiles(fullPath, "*.sln", SearchOption.TopDirectoryOnly);
            if (fileList.Length > 0)
            {
                return fileList.First();
            }
            return "";
        }
    }
}
