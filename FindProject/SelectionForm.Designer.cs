﻿namespace FindProject
{
    partial class SelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            cbProjectPathList = new ComboBox();
            lbProject = new Label();
            btnYes = new Button();
            cbxRemember = new CheckBox();
            btnExplorer = new Button();
            label1 = new Label();
            SuspendLayout();
            // 
            // cbProjectPathList
            // 
            cbProjectPathList.FormattingEnabled = true;
            cbProjectPathList.Location = new Point(76, 79);
            cbProjectPathList.Name = "cbProjectPathList";
            cbProjectPathList.Size = new Size(490, 32);
            cbProjectPathList.TabIndex = 0;
            // 
            // lbProject
            // 
            lbProject.AutoSize = true;
            lbProject.Location = new Point(12, 82);
            lbProject.Name = "lbProject";
            lbProject.Size = new Size(46, 24);
            lbProject.TabIndex = 1;
            lbProject.Text = "项目";
            // 
            // btnYes
            // 
            btnYes.BackColor = SystemColors.ControlLightLight;
            btnYes.Font = new Font("Microsoft YaHei UI", 9F, FontStyle.Bold, GraphicsUnit.Point);
            btnYes.Location = new Point(454, 127);
            btnYes.Name = "btnYes";
            btnYes.Size = new Size(112, 34);
            btnYes.TabIndex = 2;
            btnYes.Text = "打开";
            btnYes.UseVisualStyleBackColor = false;
            btnYes.Click += btnYes_Click;
            // 
            // cbxRemember
            // 
            cbxRemember.AutoSize = true;
            cbxRemember.Location = new Point(16, 131);
            cbxRemember.Name = "cbxRemember";
            cbxRemember.Size = new Size(72, 28);
            cbxRemember.TabIndex = 3;
            cbxRemember.Text = "记住";
            cbxRemember.UseVisualStyleBackColor = true;
            cbxRemember.CheckedChanged += cbxRemember_CheckedChanged;
            // 
            // btnExplorer
            // 
            btnExplorer.Location = new Point(336, 127);
            btnExplorer.Name = "btnExplorer";
            btnExplorer.Size = new Size(112, 34);
            btnExplorer.TabIndex = 4;
            btnExplorer.Text = "打开文件夹";
            btnExplorer.UseVisualStyleBackColor = true;
            btnExplorer.Click += btnExplorer_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(12, 27);
            label1.Name = "label1";
            label1.Size = new Size(550, 24);
            label1.TabIndex = 5;
            label1.Text = "提示：匹配到多个解决方案，请选择你要打开的方案，然后点击确定";
            // 
            // SelectionForm
            // 
            AutoScaleDimensions = new SizeF(11F, 24F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(578, 194);
            Controls.Add(label1);
            Controls.Add(btnExplorer);
            Controls.Add(cbxRemember);
            Controls.Add(btnYes);
            Controls.Add(lbProject);
            Controls.Add(cbProjectPathList);
            Name = "SelectionForm";
            StartPosition = FormStartPosition.CenterParent;
            Text = "匹配解决方案";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private ComboBox cbProjectPathList;
        private Label lbProject;
        private Button btnYes;
        private CheckBox cbxRemember;
        private Button btnExplorer;
        private Label label1;
    }
}