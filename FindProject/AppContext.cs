﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindProject
{
    public static class AppContext
    {
        public const int SHOW_FIND_PROJECT_WINDOW = 0x5000;

        private static IServiceProvider _serviceProvider { get; set; }
        public static void Configure(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public static T? Resolve<T>() where T : notnull
        {
            return _serviceProvider.GetService<T>();
        }

        public static IEnumerable<T> ResolveAll<T>() where T : notnull
        {
            return _serviceProvider.GetServices<T>();
        }
    }
}
