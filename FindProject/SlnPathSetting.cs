﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using static System.Windows.Forms.Design.AxImporter;

namespace FindProject
{
    public partial class SlnPathSetting : Form
    {
        public SlnPathSetting()
        {
            InitializeComponent();
            LoadContent();
        }
        public void LoadContent()
        {
            var options = new JsonSerializerOptions { WriteIndented = true };
            this.richTxtContent.Text = JsonSerializer.Serialize(AppConfig.Options.SelectedPath, options);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AppConfig.Options.SelectedPath.Clear();
            try
            {
                var data = JsonSerializer.Deserialize<List<SelectedPath>>(this.richTxtContent.Text);
                if (data == null)
                {
                    data = new List<SelectedPath>();
                }
                AppConfig.Options.SelectedPath = data;
                AppConfig.UpdateSection("SelectedPath", JsonSerializer.Serialize(data));
                MessageBox.Show("保存成功", "成功", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("这不是一个合法的JSON结构", "失败", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadContent();
            MessageBox.Show("加载成功", "成功", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void 格式化ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var data = JsonSerializer.Deserialize<List<SelectedPath>>(this.richTxtContent.Text);
                var options = new JsonSerializerOptions { WriteIndented = true };
                this.richTxtContent.Text = JsonSerializer.Serialize(data, options);
            }
            catch
            {
                MessageBox.Show("这不是一个合法的JSON结构", "失败", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
