﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindProject.Interfaces
{
    internal interface IMatchProjectPath
    {
        string MatchProject(string fullPath, string projectFolderName);
    }
}
