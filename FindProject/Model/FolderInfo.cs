﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindProject.Model
{
    internal class FolderInfo
    {
        public required string FolderName { get; set; }
        public required DateTime CreateTime { get; set; }
        public required string FolderPath { get; set; }
        public string? SvnPath { get; set; }
    }
}
