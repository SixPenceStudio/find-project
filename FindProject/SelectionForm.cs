﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FindProject
{
    public partial class SelectionForm : Form
    {
        public string SelectedPath { get; set; }
        public Dictionary<string, string> PathDic { get; set; } = new Dictionary<string, string>(); // 相对路径和完整路径
        public SelectionForm(List<string> pathList)
        {
            InitializeComponent();
            if (pathList != null && pathList.Count > 0)
            {
                foreach (var item in pathList)
                {
                    var arr = item.Split("\\");
                    PathDic.Add("..\\" + arr[arr.Length - 2] + "\\" + arr[arr.Length - 1], item);
                }
                this.cbProjectPathList.Items.AddRange(PathDic.Keys.ToArray());
            }
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            this.SelectedPath = PathDic[this.cbProjectPathList.Text];
            if (this.cbxRemember.Checked)
            {
                var folderPath = SelectedPath.Substring(0, SelectedPath.LastIndexOf("\\")); // 退回解决方案
                folderPath = folderPath.Substring(0, folderPath.LastIndexOf("\\")); // 退回项目文件夹
                var existItem = AppConfig.Options.SelectedPath.Find(item => item.ProjectPath == folderPath);
                // 记住选择路径
                if (existItem == null)
                {
                    AppConfig.Options.SelectedPath.Add(new SelectedPath() { ProjectPath = folderPath, SlnPath = this.SelectedPath });
                    AppConfig.UpdateSection("SelectedPath", AppConfig.Options.SelectedPath);
                }
            }
            this.Close();
        }

        /// <summary>
        /// 打开目标文件夹
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExplorer_Click(object sender, EventArgs e)
        {
            if (this.cbProjectPathList.Text != null && this.cbProjectPathList.Text != "")
            {
                var filePath = PathDic[this.cbProjectPathList.Text];
                var folderPath = filePath.Substring(0, filePath.LastIndexOf("\\"));
                System.Diagnostics.Process.Start("explorer.exe", folderPath);
            }
            else
            {
                MessageBox.Show("请选择你要打开的文件夹", "错误", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cbxRemember_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
