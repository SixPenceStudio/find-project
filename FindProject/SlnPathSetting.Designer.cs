﻿namespace FindProject
{
    partial class SlnPathSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SlnPathSetting));
            splitContainer1 = new SplitContainer();
            menuStrip1 = new MenuStrip();
            saveToolStripMenuItem = new ToolStripMenuItem();
            loadToolStripMenuItem = new ToolStripMenuItem();
            格式化ToolStripMenuItem = new ToolStripMenuItem();
            richTxtContent = new RichTextBox();
            ((System.ComponentModel.ISupportInitialize)splitContainer1).BeginInit();
            splitContainer1.Panel1.SuspendLayout();
            splitContainer1.Panel2.SuspendLayout();
            splitContainer1.SuspendLayout();
            menuStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // splitContainer1
            // 
            splitContainer1.Dock = DockStyle.Fill;
            splitContainer1.Location = new Point(0, 0);
            splitContainer1.Name = "splitContainer1";
            splitContainer1.Orientation = Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            splitContainer1.Panel1.Controls.Add(menuStrip1);
            // 
            // splitContainer1.Panel2
            // 
            splitContainer1.Panel2.Controls.Add(richTxtContent);
            splitContainer1.Size = new Size(800, 450);
            splitContainer1.SplitterDistance = 32;
            splitContainer1.TabIndex = 1;
            // 
            // menuStrip1
            // 
            menuStrip1.ImageScalingSize = new Size(24, 24);
            menuStrip1.Items.AddRange(new ToolStripItem[] { saveToolStripMenuItem, loadToolStripMenuItem, 格式化ToolStripMenuItem });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(800, 32);
            menuStrip1.TabIndex = 0;
            menuStrip1.Text = "menuStrip1";
            // 
            // saveToolStripMenuItem
            // 
            saveToolStripMenuItem.Image = Properties.Resources.保存;
            saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            saveToolStripMenuItem.Size = new Size(86, 28);
            saveToolStripMenuItem.Text = "保存";
            saveToolStripMenuItem.Click += saveToolStripMenuItem_Click;
            // 
            // loadToolStripMenuItem
            // 
            loadToolStripMenuItem.Image = Properties.Resources.加载;
            loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            loadToolStripMenuItem.Size = new Size(122, 28);
            loadToolStripMenuItem.Text = "重新加载";
            loadToolStripMenuItem.Click += loadToolStripMenuItem_Click;
            // 
            // 格式化ToolStripMenuItem
            // 
            格式化ToolStripMenuItem.Image = (Image)resources.GetObject("格式化ToolStripMenuItem.Image");
            格式化ToolStripMenuItem.Name = "格式化ToolStripMenuItem";
            格式化ToolStripMenuItem.Size = new Size(104, 28);
            格式化ToolStripMenuItem.Text = "格式化";
            格式化ToolStripMenuItem.Click += 格式化ToolStripMenuItem_Click;
            // 
            // richTxtContent
            // 
            richTxtContent.Dock = DockStyle.Fill;
            richTxtContent.Location = new Point(0, 0);
            richTxtContent.Name = "richTxtContent";
            richTxtContent.Size = new Size(800, 414);
            richTxtContent.TabIndex = 0;
            richTxtContent.Text = "";
            // 
            // SlnPathSetting
            // 
            AutoScaleDimensions = new SizeF(11F, 24F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(splitContainer1);
            Name = "SlnPathSetting";
            StartPosition = FormStartPosition.CenterParent;
            Text = "默认路径编辑";
            splitContainer1.Panel1.ResumeLayout(false);
            splitContainer1.Panel1.PerformLayout();
            splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer1).EndInit();
            splitContainer1.ResumeLayout(false);
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private SplitContainer splitContainer1;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem saveToolStripMenuItem;
        private ToolStripMenuItem loadToolStripMenuItem;
        private RichTextBox richTxtContent;
        private ToolStripMenuItem 格式化ToolStripMenuItem;
    }
}