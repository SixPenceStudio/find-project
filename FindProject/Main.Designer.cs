﻿namespace FindProject
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            panel1 = new Panel();
            containerHeader = new SplitContainer();
            menuStrip1 = new MenuStrip();
            设置ToolStripMenuItem = new ToolStripMenuItem();
            检索路径ToolStripMenuItem = new ToolStripMenuItem();
            默认解决方案路径ToolStripMenuItem = new ToolStripMenuItem();
            新增检索目录ToolStripMenuItem1 = new ToolStripMenuItem();
            btnOpenVS = new Button();
            btnSearch = new Button();
            btnExplorer = new Button();
            textBox1 = new TextBox();
            label1 = new Label();
            panel2 = new Panel();
            containerBody = new SplitContainer();
            splitContainer2 = new SplitContainer();
            searchResultLabel = new Label();
            treeView1 = new TreeView();
            splitContainer1 = new SplitContainer();
            label2 = new Label();
            treeView2 = new TreeView();
            imageList1 = new ImageList(components);
            panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)containerHeader).BeginInit();
            containerHeader.Panel1.SuspendLayout();
            containerHeader.Panel2.SuspendLayout();
            containerHeader.SuspendLayout();
            menuStrip1.SuspendLayout();
            panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)containerBody).BeginInit();
            containerBody.Panel1.SuspendLayout();
            containerBody.Panel2.SuspendLayout();
            containerBody.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer2).BeginInit();
            splitContainer2.Panel1.SuspendLayout();
            splitContainer2.Panel2.SuspendLayout();
            splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer1).BeginInit();
            splitContainer1.Panel1.SuspendLayout();
            splitContainer1.Panel2.SuspendLayout();
            splitContainer1.SuspendLayout();
            SuspendLayout();
            // 
            // panel1
            // 
            panel1.Controls.Add(containerHeader);
            panel1.Dock = DockStyle.Top;
            panel1.Location = new Point(0, 0);
            panel1.Name = "panel1";
            panel1.Size = new Size(1178, 85);
            panel1.TabIndex = 0;
            // 
            // containerHeader
            // 
            containerHeader.Dock = DockStyle.Fill;
            containerHeader.Location = new Point(0, 0);
            containerHeader.Name = "containerHeader";
            containerHeader.Orientation = Orientation.Horizontal;
            // 
            // containerHeader.Panel1
            // 
            containerHeader.Panel1.Controls.Add(menuStrip1);
            // 
            // containerHeader.Panel2
            // 
            containerHeader.Panel2.Controls.Add(btnOpenVS);
            containerHeader.Panel2.Controls.Add(btnSearch);
            containerHeader.Panel2.Controls.Add(btnExplorer);
            containerHeader.Panel2.Controls.Add(textBox1);
            containerHeader.Panel2.Controls.Add(label1);
            containerHeader.Size = new Size(1178, 85);
            containerHeader.SplitterDistance = 37;
            containerHeader.SplitterWidth = 3;
            containerHeader.TabIndex = 3;
            // 
            // menuStrip1
            // 
            menuStrip1.BackColor = SystemColors.ControlLight;
            menuStrip1.ImageScalingSize = new Size(28, 28);
            menuStrip1.Items.AddRange(new ToolStripItem[] { 设置ToolStripMenuItem });
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Padding = new Padding(5, 2, 0, 2);
            menuStrip1.Size = new Size(1178, 36);
            menuStrip1.TabIndex = 0;
            menuStrip1.Text = "menuStrip1";
            // 
            // 设置ToolStripMenuItem
            // 
            设置ToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[] { 检索路径ToolStripMenuItem, 默认解决方案路径ToolStripMenuItem, 新增检索目录ToolStripMenuItem1 });
            设置ToolStripMenuItem.Image = Properties.Resources.设置;
            设置ToolStripMenuItem.Name = "设置ToolStripMenuItem";
            设置ToolStripMenuItem.Size = new Size(90, 32);
            设置ToolStripMenuItem.Text = "设置";
            // 
            // 检索路径ToolStripMenuItem
            // 
            检索路径ToolStripMenuItem.Name = "检索路径ToolStripMenuItem";
            检索路径ToolStripMenuItem.Size = new Size(218, 34);
            检索路径ToolStripMenuItem.Text = "检索目录";
            检索路径ToolStripMenuItem.Click += 检索路径ToolStripMenuItem_Click;
            // 
            // 默认解决方案路径ToolStripMenuItem
            // 
            默认解决方案路径ToolStripMenuItem.Name = "默认解决方案路径ToolStripMenuItem";
            默认解决方案路径ToolStripMenuItem.Size = new Size(218, 34);
            默认解决方案路径ToolStripMenuItem.Text = "解决方案路径";
            默认解决方案路径ToolStripMenuItem.Click += 默认解决方案路径ToolStripMenuItem_Click;
            // 
            // 新增检索目录ToolStripMenuItem1
            // 
            新增检索目录ToolStripMenuItem1.Name = "新增检索目录ToolStripMenuItem1";
            新增检索目录ToolStripMenuItem1.Size = new Size(218, 34);
            新增检索目录ToolStripMenuItem1.Text = "新增检索目录";
            新增检索目录ToolStripMenuItem1.Click += 新增检索目录ToolStripMenuItem1_Click;
            // 
            // btnOpenVS
            // 
            btnOpenVS.Image = (Image)resources.GetObject("btnOpenVS.Image");
            btnOpenVS.ImageAlign = ContentAlignment.MiddleLeft;
            btnOpenVS.Location = new Point(918, 3);
            btnOpenVS.Margin = new Padding(5);
            btnOpenVS.Name = "btnOpenVS";
            btnOpenVS.Size = new Size(112, 34);
            btnOpenVS.TabIndex = 9;
            btnOpenVS.Text = "打开项目";
            btnOpenVS.TextAlign = ContentAlignment.MiddleRight;
            btnOpenVS.UseVisualStyleBackColor = true;
            btnOpenVS.Click += btnOpenVS_Click;
            // 
            // btnSearch
            // 
            btnSearch.Location = new Point(565, 4);
            btnSearch.Name = "btnSearch";
            btnSearch.Size = new Size(111, 34);
            btnSearch.TabIndex = 2;
            btnSearch.Text = "查询";
            btnSearch.UseVisualStyleBackColor = true;
            btnSearch.Click += btnSearch_Click;
            // 
            // btnExplorer
            // 
            btnExplorer.Image = (Image)resources.GetObject("btnExplorer.Image");
            btnExplorer.ImageAlign = ContentAlignment.MiddleLeft;
            btnExplorer.Location = new Point(1038, 3);
            btnExplorer.Name = "btnExplorer";
            btnExplorer.Size = new Size(130, 34);
            btnExplorer.TabIndex = 8;
            btnExplorer.Text = "打开文件夹";
            btnExplorer.TextAlign = ContentAlignment.MiddleRight;
            btnExplorer.UseVisualStyleBackColor = true;
            btnExplorer.Click += btnExplorer_Click;
            // 
            // textBox1
            // 
            textBox1.Location = new Point(188, 6);
            textBox1.Name = "textBox1";
            textBox1.PlaceholderText = "请输入项目关键词，例如：苏州";
            textBox1.Size = new Size(371, 30);
            textBox1.TabIndex = 1;
            textBox1.KeyPress += textBox1_KeyPress;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(13, 8);
            label1.Name = "label1";
            label1.Size = new Size(172, 24);
            label1.TabIndex = 0;
            label1.Text = "项目名称（模糊）：";
            // 
            // panel2
            // 
            panel2.Controls.Add(containerBody);
            panel2.Dock = DockStyle.Fill;
            panel2.Location = new Point(0, 85);
            panel2.Name = "panel2";
            panel2.Size = new Size(1178, 659);
            panel2.TabIndex = 1;
            // 
            // containerBody
            // 
            containerBody.BorderStyle = BorderStyle.FixedSingle;
            containerBody.Dock = DockStyle.Fill;
            containerBody.Location = new Point(0, 0);
            containerBody.Name = "containerBody";
            // 
            // containerBody.Panel1
            // 
            containerBody.Panel1.Controls.Add(splitContainer2);
            // 
            // containerBody.Panel2
            // 
            containerBody.Panel2.Controls.Add(splitContainer1);
            containerBody.Size = new Size(1178, 659);
            containerBody.SplitterDistance = 594;
            containerBody.SplitterWidth = 3;
            containerBody.TabIndex = 0;
            // 
            // splitContainer2
            // 
            splitContainer2.Dock = DockStyle.Fill;
            splitContainer2.Location = new Point(0, 0);
            splitContainer2.Name = "splitContainer2";
            splitContainer2.Orientation = Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            splitContainer2.Panel1.Controls.Add(searchResultLabel);
            // 
            // splitContainer2.Panel2
            // 
            splitContainer2.Panel2.Controls.Add(treeView1);
            splitContainer2.Size = new Size(592, 657);
            splitContainer2.SplitterDistance = 76;
            splitContainer2.SplitterWidth = 3;
            splitContainer2.TabIndex = 1;
            // 
            // searchResultLabel
            // 
            searchResultLabel.AutoSize = true;
            searchResultLabel.Location = new Point(12, 23);
            searchResultLabel.Name = "searchResultLabel";
            searchResultLabel.Size = new Size(118, 24);
            searchResultLabel.TabIndex = 0;
            searchResultLabel.Text = "搜索结果：无";
            // 
            // treeView1
            // 
            treeView1.Dock = DockStyle.Fill;
            treeView1.Location = new Point(0, 0);
            treeView1.Name = "treeView1";
            treeView1.Size = new Size(592, 578);
            treeView1.TabIndex = 0;
            treeView1.NodeMouseClick += treeView1_NodeMouseClick;
            treeView1.NodeMouseDoubleClick += treeView1_NodeMouseDoubleClick;
            // 
            // splitContainer1
            // 
            splitContainer1.Dock = DockStyle.Fill;
            splitContainer1.Location = new Point(0, 0);
            splitContainer1.Name = "splitContainer1";
            splitContainer1.Orientation = Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            splitContainer1.Panel1.Controls.Add(label2);
            // 
            // splitContainer1.Panel2
            // 
            splitContainer1.Panel2.Controls.Add(treeView2);
            splitContainer1.Size = new Size(579, 657);
            splitContainer1.SplitterDistance = 76;
            splitContainer1.TabIndex = 0;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(17, 23);
            label2.Name = "label2";
            label2.Size = new Size(64, 24);
            label2.TabIndex = 0;
            label2.Text = "项目：";
            // 
            // treeView2
            // 
            treeView2.Dock = DockStyle.Fill;
            treeView2.Location = new Point(0, 0);
            treeView2.Name = "treeView2";
            treeView2.Size = new Size(579, 577);
            treeView2.TabIndex = 0;
            treeView2.NodeMouseClick += treeView2_NodeMouseClick;
            treeView2.NodeMouseDoubleClick += treeView2_NodeMouseDoubleClick;
            // 
            // imageList1
            // 
            imageList1.ColorDepth = ColorDepth.Depth8Bit;
            imageList1.ImageStream = (ImageListStreamer)resources.GetObject("imageList1.ImageStream");
            imageList1.TransparentColor = Color.Transparent;
            imageList1.Images.SetKeyName(0, "folder.png");
            imageList1.Images.SetKeyName(1, "Visual Studio.png");
            imageList1.Images.SetKeyName(2, "document.png");
            // 
            // Main
            // 
            AutoScaleDimensions = new SizeF(11F, 24F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1178, 744);
            Controls.Add(panel2);
            Controls.Add(panel1);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "Main";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "FindProject";
            Shown += Main_Shown;
            panel1.ResumeLayout(false);
            containerHeader.Panel1.ResumeLayout(false);
            containerHeader.Panel1.PerformLayout();
            containerHeader.Panel2.ResumeLayout(false);
            containerHeader.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)containerHeader).EndInit();
            containerHeader.ResumeLayout(false);
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            panel2.ResumeLayout(false);
            containerBody.Panel1.ResumeLayout(false);
            containerBody.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)containerBody).EndInit();
            containerBody.ResumeLayout(false);
            splitContainer2.Panel1.ResumeLayout(false);
            splitContainer2.Panel1.PerformLayout();
            splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer2).EndInit();
            splitContainer2.ResumeLayout(false);
            splitContainer1.Panel1.ResumeLayout(false);
            splitContainer1.Panel1.PerformLayout();
            splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer1).EndInit();
            splitContainer1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private Panel panel1;
        private Panel panel2;
        private Button btnSearch;
        private TextBox textBox1;
        private Label label1;
        private SplitContainer containerBody;
        private SplitContainer splitContainer2;
        private Label searchResultLabel;
        private TreeView treeView1;
        private SplitContainer containerHeader;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem 设置ToolStripMenuItem;
        private TextBox txtProjectName;
        private Label label2;
        private Label label4;
        private Label label3;
        private TextBox txtProjectCreateTime;
        private TextBox txtProjectPath;
        private Button btnExplorer;
        private ToolStripMenuItem 检索路径ToolStripMenuItem;
        private Button btnOpenVS;
        private ToolStripMenuItem 默认解决方案路径ToolStripMenuItem;
        private Label label5;
        private TextBox textBox2;
        private SplitContainer splitContainer1;
        private TreeView treeView2;
        private ImageList imageList1;
        private ToolStripMenuItem 新增检索目录ToolStripMenuItem1;
    }
}